Welcome to Chain of Conflict!
=============================

This is the project page for **Chain Of Conflict** on Bitbucket.  You'll be able to see live commits from the CoC developers along with integrated code submissions from the community! 

We're working hard to get a basic version of Chain of Conflict deathmatch up and running to provide a basis for design iteration and further development. You can follow our progress on this fork, and start extending or modding the code as it becomes functional. If you want to get involved, join us on our [forums](http://coin2talk.com) to participate in design discussions, find projects that need help, and discover other opportunities to contribute.



How to get started
-------------------

The current Chain of Conflict project is based on Unreal Engine version 4.3.0.  You can always get this version of the engine using the [Unreal Engine Launcher](https://www.unrealengine.com/dashboard), or you can grab the [full engine source](https://github.com/EpicGames/UnrealEngine/releases/tag/4.3.0-release) from GitHub and compile it yourself.

**Downloading the Chain of Conflict project:**

- Download the project by clicking the **Download Source** button on this page.
- Unzip the files to a folder on your computer.  
- Load up [Unreal Editor]
- On the Project Browser screen, click **Browse** and navigate to the folder where unzipped the files.
- Choose the **ShooterGame.uproject** file.  

You'll now be looking at the very latest work-in-progress version of the game!



Setting up the project
---------------------

**Windows:**
 
 1. Be sure to have [Visual Studio 2013](http://www.microsoft.com/en-us/download/details.aspx?id=40787) installed.  You can use any desktop version of Visual Studio 2013, including the free version:  [Visual Studio 2013 Express for Windows Desktop](http://www.microsoft.com/en-us/download/details.aspx?id=40787)
 2. right click on **ShooterGame.uproject** to generate C++ project files.
 3. You can now open up **ShooterGame.uproject** in the editor!
 
**Mac OSX:**
  
1. Be sure to have [Xcode 5.1](https://itunes.apple.com/us/app/xcode/id497799835) installed.
2. right click on on **ShooterGame.uproject** > services and click on **Generate xcode project** to generate C++ project files.
3. Open **ShooterGame.codeproj** and select **ShooterGameEditor - Mac** from the scheme selector..
4. You can now open up **ShooterGame.uproject** in the editor!
